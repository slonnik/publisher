import base64
from pathlib import Path
import uuid
import time
import requests
import json
import config
import threading
import logging
from logging.handlers import RotatingFileHandler
from fsmonitor import FSMonitor

log_formatter = logging.Formatter('%(asctime)s %(levelname)s %(funcName)s(%(lineno)d) %(message)s')
log_file_handler = RotatingFileHandler(config.LOG_FILE, mode='a', maxBytes=config.LOG_FILE_MAX_SIZE,
                                 backupCount=2, encoding=None, delay=0)
log_console_handler = logging.StreamHandler()
log_console_handler.setFormatter(log_formatter)
log_console_handler.setLevel(logging.DEBUG)
log_file_handler.setFormatter(log_formatter)
log_file_handler.setLevel(logging.INFO)

log = logging.getLogger('root')
log.setLevel(logging.DEBUG)
log.addHandler(log_file_handler)
log.addHandler(log_console_handler)


def pass_file(data):
    json_data = json.loads(data)
    base64_data = json_data.get('data')
    raw_data = base64.b64decode(base64_data)
    path = Path(config.ARCHIVE_PATH).joinpath(json_data.get('name'))
    log.info("save %s", path)
    with open(path, 'wb') as binary_file:
        binary_file.write(raw_data)
    log.info('Succeeded to save "%s" with id "%s"', json_data.get('name'), json_data.get('id'))

def pass_request(data):
    requests.post("http://%s:%s/" % (config.TARGET_HOST, config.TARGET_PORT), json=data)

if __name__ == '__main__':
    log.info('Start process')
    try:
        m = FSMonitor()
        watch = m.add_dir_watch(config.ROOT_FOLDER)
        while True:
            for evt in m.read_events():
                if 'create' == evt.action_name:
                    try:
                        with Path(config.ROOT_FOLDER).joinpath(evt.name) as path:
                            log.info('File to process "%s"', path.absolute())
                            raw_data = path.read_bytes()
                            base64_string = base64.b64encode(raw_data).decode('utf8')
                            json_data = {'id': str(uuid.uuid4()), 'data': base64_string, 'name': path.name,
                                         'time': str(path.stat().st_ctime)}
                            t = threading.Thread(target=pass_request, args=(json.dumps(json_data),))
                            t.start()
                    except Exception as ex:
                        log.error(ex)
                        pass
            time.sleep(1)
    except KeyboardInterrupt:
        log.info('Process is interrupted')
    log.info('End process')