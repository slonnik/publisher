from flask import Flask, request
import json
import base64
import config
from pathlib import Path
app = Flask(__name__)

@app.route('/', methods=['POST'])
def add_image():
    json_data = json.loads(request.get_json(silent=True))
    base64_data = json_data.get('data')
    raw_data = base64.b64decode(base64_data)
    path = Path(config.ARCHIVE_PATH).joinpath(json_data.get('name'))
    with open(path, 'wb') as binary_file:
        binary_file.write(raw_data)
    return 'ok'

app.run(host=config.TARGET_HOST, port=config.TARGET_PORT)